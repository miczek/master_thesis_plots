import argparse
import copy
import os
import re
import signal
import subprocess
import sys
import time
from typing import List

import colorama
import psutil
import rospy
import yaml
from sensor_msgs.msg import Image

list_of_processes = []


def print_yellow(text: str) -> None:
    print(colorama.Fore.YELLOW + text + colorama.Style.RESET_ALL)


def sigint_handler(*_) -> None:
    print(colorama.Fore.RED + "Terminating..." + colorama.Style.RESET_ALL)
    global list_of_processes
    for process in list_of_processes:
        parent = psutil.Process(process.pid)
        for child in parent.children(recursive=True):
            child.kill()
        parent.kill()
    sys.exit(0)


def get_args() -> argparse.Namespace:
    parser = argparse.ArgumentParser(
        description='This script automates the execution of multiple runs of VIO algorithms on specified datasets')
    parser.add_argument('config', help='local config yaml filename')
    return parser.parse_args()


def get_next_free_index(index_list: List[int]) -> int:
    index_list.sort()
    expected_index = 1
    for index in index_list:
        if index != expected_index:
            return expected_index
        expected_index += 1
    return expected_index


# This callback used as a heartbeat to check if the dataset has terminated.
# If the image_received flag isn't updated within a certain period, the script assumes the execution finished,
# terminates the current run and proceeds with further executions
def heartbeat_image_callback(msg: Image) -> None:
    global image_received
    image_received = True


signal.signal(signal.SIGINT, sigint_handler)
args = get_args()

with open(args.config, 'r') as stream:
    try:
        config_list = yaml.safe_load(stream)
    except yaml.YAMLError as exc:
        print(exc)
        exit(1)

for config in config_list:
    result_folder_pattern = re.compile(f'^{config["result_folder_prefix"]}(\d+)$')
    saved_commands = copy.deepcopy(config['commands'])

    # Find all folders that already have a number assigned
    # It assumes that each previous run generated a directory in form result_object_prefix_N, where N is the run ID
    os.makedirs(config['result_path'], exist_ok=True)
    ls_result = os.listdir(config['result_path'])

    # List of found run indices that have already been conducted
    run_indices = []
    for elem in ls_result:
        match = result_folder_pattern.match(elem)
        if os.path.isdir(os.path.join(config['result_path'], elem)) and match:
            run_indices.append(int(match.groups(0)[0]))
    requested_runs = config['requested_runs']
    while True:
        if len(run_indices) >= requested_runs:
            print(f'There are already {len(run_indices)} results available, '
                  f'at least as many as you requested ({requested_runs}).\n'
                  f'Please increase the parameter "requested_runs" if you need more.')
            break

        # Get the next available index of the result folder
        next_index = get_next_free_index(run_indices)
        print(next_index)
        next_result_folder_name = config["result_folder_prefix"] + str(next_index)
        next_result_folder_path = os.path.join(config['result_path'], next_result_folder_name)
        run_indices.append(next_index)
        print(next_result_folder_path)
        os.mkdir(next_result_folder_path)
        # Whether the logger command needs substitution of the result folder
        extend = config['extend_commands_with_result_path_folder']
        if extend:
            for index, command in enumerate(saved_commands):
                # Format all {result_path_folder} entries in strings, setting them to the destination folder name
                if 'result_path_folder' in command:
                    config['commands'][index] = saved_commands[index].format(result_path_folder=next_result_folder_path)
        list_of_processes = []
        # Roscore is an essential part of every algorithm because they all run in ROS
        list_of_processes.append(subprocess.Popen(['/bin/bash', '-c', 'roscore']))
        time.sleep(5.0)
        for command in config['commands']:
            print('Executing ' + command)
            list_of_processes.append(subprocess.Popen(['/bin/bash', '-c', command]))
        time.sleep(5.0)
        if 'termiate_if_no_images' in config and config['termiate_if_no_images']:
            list_of_processes.append(subprocess.Popen(['/bin/bash', '-c', config["rosbag_command"]]))
            # Global variable image_received serves as a heartbeat and is checked every 10s.
            image_received = False
            rospy.init_node('hearbeat_check')
            image_sub = rospy.Subscriber(name=config['image_topic'],
                                         data_class=Image, callback=heartbeat_image_callback)
            time.sleep(10.0)

            while image_received:
                print_yellow("Images are still being published, not terminating")
                image_received = False
                # If during the sleep time an image arrives, heartbeat_image_callback sets the value of image_received
                # to true. Otherwise it's assumed that the dataset has terminated.
                time.sleep(1.0)

            print_yellow("Images stopped being published, terminating")
            image_sub.unregister()
        else:
            # Alternatively, if playing a rosbag, just block until the command returns
            subprocess.call(['/bin/bash', '-c', config["rosbag_command"]])

        # Clean up before the next run
        for process in list_of_processes:
            parent = psutil.Process(process.pid)
            for child in parent.children(recursive=True):
                child.kill()
            parent.kill()
