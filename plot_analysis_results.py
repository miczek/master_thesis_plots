# Visualize the results of reproducibility analysis
import pandas as pd
import matplotlib.pyplot as plt
import yaml

with open('result.yaml', 'r') as stream:
    try:
        result_list = yaml.safe_load(stream)
    except yaml.YAMLError as exc:
        print(exc)
        exit(1)
result_folders = ['/home/mikolaj/Projects/plot_results/results/Vins/szczecin_small_loop',
                  '/home/mikolaj/Projects/plot_results/results/rovio/szczecin_small_loop',
                  '/home/mikolaj/Projects/plot_results/results/openVins/szczecin_small_loop']
algorithm_names = ['VINS-Mono', 'ROVIO', 'OpenVins']
for result_folder, algorithm_name in zip(result_folders, algorithm_names):
    successful_runs = result_list[result_folder]['successful_runs']
    list_of_ate_values = []
    for run in successful_runs:
        # Append the RMSE values of successful runs to the list
        list_of_ate_values.append(successful_runs[run]['ate_rmse_value'])
    vins_ate_series = pd.Series(list_of_ate_values)
    # Create a histogram of the RMSEs
    plot = vins_ate_series.hist()
    plot.set_title(f'ATEs of single runs against the mean trajectory for {algorithm_name}')
    plot.set_xlabel('ATE [m]')
    plot.set_ylabel('number of samples')
    plt.plot([], [], ' ', label=f'Mean = {vins_ate_series.mean():.2f} m')
    plt.plot([], [], ' ', label=f'Median = {vins_ate_series.median():.2f} m')
    plt.legend()
    # Add a number above each bar of the histogram
    for bar in plot.patches:
        height = bar.get_height()
        plot.annotate(f'{int(height)}', xy=(bar.get_x()+bar.get_width()/2, height),
                      xytext=(0, 5), textcoords='offset points', ha='center', va='bottom')
    # Increase the plotting box height by a little to ensure the numbers fit inside of it
    plot.set_ylim(plot.get_ylim()[0], plot.get_ylim()[1]*1.05)
    plt.show()
