import pandas as pd
import matplotlib.pyplot as plt
plot_config = [{'name': 'VINS-Mono',
                'filename': 'vins_mono_resource_usage_reeval.csv'},
               {'name': 'OpenVins',
                'filename': 'openvins_resource_usage_reeval.csv'},
               {'name': 'ROVIO',
                'filename': 'rovio_resource_usage_reeval.csv'}]
max_cpu = 400
fig, ax = plt.subplots(2)

ax[0].set_title('Usage of resources')
ax[0].set_ylabel(f'% of one CPU (max {max_cpu})')
ax[1].set_ylabel('RAM usage [MB]')
ax[1].set_xlabel('time [s]')
for resource_usage in plot_config:
    result_df = pd.read_csv(resource_usage['filename'])
    print(result_df)
    ax[0].plot(result_df['timestamp'], result_df['cpu'], label=resource_usage['name'])
    ax[1].plot(result_df['timestamp'], result_df['ram_mb'], label=resource_usage['name'])
plt.legend()
# plt.show()
# exit()
fig.savefig('test_resource_usage.svg', format='svg')
