# Evaluation framework for VIO algorithms

This repo contains a set of scripts which automate the evaluation of VIO algorithms.
It is part of my Master's thesis, I'll link it once it has been published.

## Instructions
It is recommended to do everything within an Ubuntu virtual machine. It was tested on Ubuntu 20.04.

To start, run the script `setup_pc.bash`:

```
$ ./setup_pc.bash
```

It will install all necessary dependencies, among others ROS and the `vio-tests` repository containing all three tested algorithms.


The next step involves building the algorithms:

```
$ cd ../vio-tests # The repo vio-tests should have been cloned outside of this repo, therefore this step back
$ source ~/.bashrc
$ catkin init
$ catkin config --cmake-args -DCMAKE_BUILD_TYPE=RelWithDebInfo
$ catkin build
```

Then, install conda (e.g. with [Anaconda3 installer](https://www.anaconda.com/products/individual)).
After the installation, reopen the terminal and run `conda config --set auto_activate_base false` to disable activating the base environment every time at startup.

Then, extract the environment of this framework:

```
$ cd /path/to/this/repo
$ conda env create -f environment.yml
```

Then, activate the environment and manually add the custom plotting library to pip:

```
$ conda activate cartopy_env
$ pip install ./cartopy_plot
```

After that, install the `evo` tool to finish the setup:

```
$ cd /somewhere/else
$ git clone https://github.com/MichaelGrupp/evo.git
$ cd evo
$ pip install --editable . --upgrade --no-binary evo
```

At this stage, the environment is ready and now only the framework has to be configured properly.

### Framework setup
#### Automated algorithm execution
For the automated algorithm execution, the file `config/running_datasets/multiple_tests_config.yaml` has to be adjusted.

For each entry in `config/running_datasets/multiple_tests_config.yaml`, adjust the following properties:

```
result_path - to point where the generated results should be stored
requested_runs - how many times the algorithm should be run (keep it small to check if everything works, you can increase it later and rerun the script)
commands - adjust the part which comes after source so that it sources the right file
rosbag_command - adjust the path of the rosbag that should be played 
```

Because ROS doesn't work well with conda, make sure you deactivate any active environments with `conda deactivate` before the next step. The environment will be needed for the next steps, but not for the result generation.

I recommend downloading the bagfile `test.bag` from the dataset `szczecin_small_loop`. It can be found on [Google Drive](https://drive.google.com/drive/u/1/folders/1TDpbKOjksa0Qi8Yyj8W9iJxqCCqzYqSV).

After that, you can execute `python3 run_datasets.py config/running_datasets/multiple_tests_config.yaml` and see the algorithms in action.

#### Replication of plot configs
This is actually a legacy feature but I kept it because I ran out of time.

Basically, every single plot requires a separate plot config file, for example to specify the angle to align the ground truth with the estimate. For multiple runs of the same algorithm on the same dataset, however, one and the same configuration can be used.
Therefore, to keep it simple, the single plot config gets copied to every result subfolder to satisfy the framework.

To configure which config should land where, edit the `config/preparation/plot_config_replication.yaml` file.
For each entry, adjust the following parameters:

```
result_path - same as previously, where the generated results (multiple runs) are stored
plot_config_to_replicate - which plot config to take
```

Once this is set, run `python3 replicate_plot_configs.py config/preparation/plot_config_replication.yaml` to perform this step.

#### Visualizing the single results
This step plots the estimated trajectory along GPS for every single result.
In addition, the trajectories also get stored in TUM format because it's required by the `evo` tool.

To prepare for this step, edit the `config/plotting/all_plots_config.yaml`:

```
list_of_directories - which results should be plotted
plot_script_path - absolute path to the plotting script
```

Before you proceed, you also have to manually copy the GPS data file to every subfolder with results.
The GPS data is the file named `data_gps.txt` and is located in root of every dataset folder on the [Google Drive](https://drive.google.com/drive/u/1/folders/1TDpbKOjksa0Qi8Yyj8W9iJxqCCqzYqSV). Also the corresponding rosbag files can be found there.

The file needs to be copied to every destination where the results of an algorithm are generated. For instance, if you run 3 algorithms on the same dataset, you have to copy it 3 times to the result directory of the corresponding algorithm.
If the algorithms are named `Vins`, `openVins` and `rovio`, the same file should be copied to 3 locations:
```
/path_to_generated_results/Vins/szczecin_small_loop/run_1/../data_gps.txt
/path_to_generated_results/openVins/szczecin_small_loop/run_1/../data_gps.txt
/path_to_generated_results/rovio/szczecin_small_loop/run_1/../data_gps.txt
```

Once the GPS data files are copied, the plots can be created with

```
$ conda activate cartopy_env
$ python3 plot_results_one_by_one.py config/plotting/all_plots_config.yaml
```

This should result in plotting the GPS vs estimated trajectory for every single run of each algorithm.

This step shouldn't be skipped because it also generates the trajectory files in TUM format which are requred by the evaluation step.

#### Evaluation and generation of summary plots
In this step, the mean trajectory gets calculated and the trajectory error is computed with the `evo` tool.

Every entry in `config/evaluation/result_analysis.yaml` needs to be adjusted in the following way:

```
main_result_folder - just like the result_path, points to the destination with multiple generated results for one algorithm running on a dataset
plot_config_file - here the general config file can be taken, therefore just adjust its location by adjusting the username and location of this repo
mean_trajectory_script - same modification as above
````

Start the analysis with

```
$ python3 perform_evaluations.py config/evaluation/result_analysis.yaml
```

In addition to the plots generated in each dataset folder, also a short summary gets stored in a `result.yaml` file where the script was executed.

#### CPU usage analysis
The script `measure_cpu_usage.py` is able to log the RAM and CPU usage of the process it's monitoring.

It receives several arguments:

```
command - the process which is being monitored
exclude-list - if a child process of the monitored process contains an element from this list, it will not be counted, e.g. rviz can be ignored that way
output-file - where the result should be stored
```
so a valid command would be for example:

```
$ python3 measure_cpu_usage.py 'source /home/mikolaj/vio-tests/devel/setup.bash && roslaunch rovio rovio_node.launch' --output-file rovio_resource_usage.txt
```
