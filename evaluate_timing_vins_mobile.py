
import re
import pandas as pd
import matplotlib.pyplot as plt
import os


def get_last_float_from_string(string: str) -> float:
    return float(re.findall(f'[-+]?\d*\.\d+|\d+', string)[-1])


filename = 'results/vins-mobile/pixel_saarstr_small_round_debug/adb_log.txt'

important_strings = {'feature_tracker': 'the whole OnImageAvailableListener took',
                     'ceres_solver': 'TIMER_main_loop_processing'}

feature_tracker_lines = []
ceres_solver_lines = []


with open(filename, 'r') as file:
    print(f'reading {filename}')
    lines = file.readlines()

for line in lines:
    if important_strings['feature_tracker'] in line:
        feature_tracker_lines.append(line.rstrip())
    elif important_strings['ceres_solver'] in line:
        ceres_solver_lines.append(line.rstrip())

feature_tracker_times = []
ceres_solver_times = []
for line in feature_tracker_lines:
    feature_tracker_times.append(get_last_float_from_string(line))

for line in ceres_solver_lines:
    ceres_solver_times.append(get_last_float_from_string(line))

feature_tracker_series = pd.Series(feature_tracker_times)
ceres_times_series = pd.Series(ceres_solver_times)
plot = feature_tracker_series.hist()
plot.set_title('Distribution of feature tracker timing for VINS-Mobile')
plot.set_xlabel('Processing times [ms]')
plot.set_ylabel('number of samples')
plt.plot([], [], ' ', label=f'Mean = {feature_tracker_series.mean():.2f} ms')
plt.plot([], [], ' ', label=f'Median = {feature_tracker_series.median():.2f} ms')
plt.legend()
for bar in plot.patches:
    height = bar.get_height()
    plot.annotate(f'{int(height)}', xy=(bar.get_x()+bar.get_width()/2, height),
                  xytext=(0, 5), textcoords='offset points', ha='center', va='bottom')
plot.set_ylim(plot.get_ylim()[0], plot.get_ylim()[1]*1.05)
plt.show()

plot = ceres_times_series.hist()
plot.set_title('Distribution of ceres solver timing for VINS-Mobile')
plot.set_xlabel('Processing times [ms]')
plot.set_ylabel('number of samples')
plt.plot([], [], ' ', label=f'Mean = {ceres_times_series.mean():.2f} ms')
plt.plot([], [], ' ', label=f'Median = {ceres_times_series.median():.2f} ms')
plt.legend()
for bar in plot.patches:
    height = bar.get_height()
    plot.annotate(f'{int(height)}', xy=(bar.get_x()+bar.get_width()/2, height),
                  xytext=(0, 5), textcoords='offset points', ha='center', va='bottom')
plot.set_ylim(plot.get_ylim()[0], plot.get_ylim()[1]*1.05)
plt.show()
print(f'{ceres_times_series.max()=}')
print(f'{ceres_times_series.std()=}')
plot = plt.plot(feature_tracker_series)
plt.show()
