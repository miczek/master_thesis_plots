# Run over a list of directories: check if the single runs diverged, create a summary and store it in a yaml file

import argparse
from functools import cmp_to_key
from typing import Match, Optional
import colorama
import numpy as np
import os
import pandas as pd
import re
import subprocess
import yaml


def get_args() -> argparse.Namespace:
    parser = argparse.ArgumentParser(description='Evaluation of algorithm results')
    parser.add_argument('config', help='local config yaml filename')
    return parser.parse_args()


def compare(name1: str, name2: str) -> int:
    # Replace the default comparison method to always sort the directories by the number in the suffix
    pattern = re.compile(f'^{config["result_folder_prefix"]}(\d+)$')
    num1 = int(pattern.match(name1).groups(0)[0])
    num2 = int(pattern.match(name2).groups(0)[0])
    return num1 - num2


args = get_args()
with open(args.config, 'r') as stream:
    try:
        config_list = yaml.safe_load(stream)
    except yaml.YAMLError as exc:
        print(exc)
        exit(1)

# 1. Detect diverged and successful trajectories

# 1.1 calculate the difference between start and stop - if it exceeds a certain threshold, it diverged, e.g. if it's greater than 1/4 of the dataset length
output_dict = {}
for config in config_list:
    main_directory = config['main_result_folder']
    main_directory_content = os.listdir(main_directory)
    list_of_runs = []
    for elem in main_directory_content:
        if config['result_folder_prefix'] in elem:
            list_of_runs.append(elem)
    list_of_runs.sort(key=cmp_to_key(compare))
    list_of_non_diverged_runs = []
    list_of_diverged_runs = []
    for run_name in list_of_runs:
        run_dir_content = os.listdir(os.path.join(main_directory, run_name))
        trajectory = pd.read_csv(os.path.join(main_directory, run_name,
                                 config['trajectory_tum_format_file_name']), sep=' ')
        first = trajectory.iloc[0]
        last = trajectory.iloc[-1]
        dist = np.sqrt((last.x - first.x)**2 + (last.y - first.y)**2 + (last.z - first.z)**2)
        if dist < config['ground_truth_length'] / 3:
            list_of_non_diverged_runs.append(run_name)
        else:
            list_of_diverged_runs.append(run_name)

# 2. Run reproducibility analysis which should also generate the mean trajectory in a file
# 2.1 Create a yaml file which will be a config file for the reproducibility analysis
    reproducibility_config = {}
    with open(config['plot_config_file'], 'r') as stream:
        try:
            single_plot_config = yaml.safe_load(stream)
        except yaml.YAMLError as exc:
            print(exc)
            exit(1)
    reproducibility_config['odom_file'] = single_plot_config['odom_filename']
    reproducibility_config['odom_delimitter'] = single_plot_config['odom_delimitter']
    reproducibility_config['gps_file'] = config['gps_file_name']
    reproducibility_config['rotation_correction_deg'] = single_plot_config['subpath_correction'][0][1]
    reproducibility_config['algorithm_name'] = single_plot_config['algorithm_name']
    reproducibility_config['folders'] = list_of_non_diverged_runs
    reproducibility_config['mean_plot_filename'] = os.path.join(main_directory, "reproducibility_plot.jpg")
    reproducibility_config['mean_trajectory_output_file'] = os.path.join(
        main_directory, config["mean_trajectory_file_name"])
    reproducibility_config['odom_timestamp_units'] = single_plot_config["odom_timestamp_units"]
    reproducibility_config['main_directory'] = main_directory
    reproducibility_config_file = os.path.join(main_directory, 'reproducibility_config.yaml')
    with open(reproducibility_config_file, "w") as f:
        yaml.dump(reproducibility_config, f)
    reproducibility_cmd = (
        f'python3 {config["mean_trajectory_script"]}'
        f' --dir {main_directory} --config {reproducibility_config_file}'
    )
    print(colorama.Fore.YELLOW + reproducibility_cmd + colorama.Style.RESET_ALL)
    subprocess.call(['/bin/bash', '-c', reproducibility_cmd])

# 3. Run ATE/RTE on chosen trajectories
# 3.1 On each generated and successful (not diverged) trajectory against the mean, store the result internally
    result_dict = {}
    result_dict['successful_runs'] = {}
    result_dict['unsuccessful_runs'] = list_of_diverged_runs
    rte_delta = 20  # meters
    for run in list_of_non_diverged_runs:
        result_dict['successful_runs'][run] = {}
        evaluation_cmd = (
            f'evo_ape tum -r point_distance -a {os.path.join(main_directory, config["mean_trajectory_file_name"])} '
            f'{os.path.join(main_directory, run, config["trajectory_tum_format_file_name"])} --t_max_diff 5 -v'
        )
        print(colorama.Fore.YELLOW + evaluation_cmd + colorama.Style.RESET_ALL)
        process = subprocess.run(['/bin/bash', '-c', evaluation_cmd], capture_output=True)
        try:
            output = process.stdout.decode('utf-8')
            if 'error' in output or process.returncode != 0:
                print(colorama.Fore.RED + 'error' + colorama.Style.RESET_ALL)
        except UnicodeDecodeError as error:
            print(error)
        output_lines = output.splitlines()
# 3.1.1 analyze the output of evo with re - extract the RMSE line
        rmse_pattern = re.compile('^\s*rmse\s*(?P<rmse>\d+.\d+).*')
        rmse_match: Optional[Match] = None
        for line in output_lines:
            match = rmse_pattern.match(line)
            if match:
                rmse_match = match
                break
        if not rmse_match:
            print(colorama.Fore.RED + output + colorama.Style.RESET_ALL)
            exit(1)
        rmse_value = rmse_match.group('rmse')
        print(colorama.Fore.GREEN + rmse_value + colorama.Style.RESET_ALL)
        result_dict['successful_runs'][run] = {'ate_rmse_value': float(rmse_value)}
    print(result_dict)
# 3.2 Calculate RTE of the mean trajectory against GPS, with scale correction factor
    mean_trajectory_file_path = os.path.join(main_directory, config["mean_trajectory_file_name"])
    gps_file_path = os.path.join(main_directory, config['gps_tum_file_name'])
    mean_evaluation_cmd = (
        f'evo_rpe tum -r point_distance -a {gps_file_path} {mean_trajectory_file_path}'
        f' -v --t_max_diff 5 -d {rte_delta} -u m -p'
    )
    print(colorama.Fore.YELLOW + mean_evaluation_cmd + colorama.Style.RESET_ALL)
    process = subprocess.run(['/bin/bash', '-c', mean_evaluation_cmd], capture_output=True)
    try:
        output = process.stdout.decode('utf-8')
        if 'error' in output or process.returncode != 0:
            print(colorama.Fore.RED + 'error' + colorama.Style.RESET_ALL)
    except UnicodeDecodeError as error:
        print(error)
    output_lines = output.splitlines()

# 3.2.1 analyze the output of evo with re - extract the necessary lines
    mean_pattern = re.compile('^\s*mean\s*(?P<mean>\d+.\d+).*')
    scale_correction_pattern = re.compile('^\s*Scale correction:\s*(?P<scale_correction>\d+.\d+).*')
    mean_match: Optional[Match] = None
    scale_correction_match: Optional[Match] = None
    for line in output_lines:
        match = mean_pattern.match(line)
        if match:
            mean_match = match
            continue
        match = scale_correction_pattern.match(line)
        if match:
            scale_correction_match = match
        if mean_match and scale_correction_match:
            break

    if not mean_match or not scale_correction_match:
        print(colorama.Fore.RED + output + colorama.Style.RESET_ALL)
        exit(1)
    mean_value = mean_match.group('mean')
    scale_correction_value = scale_correction_match.group('scale_correction')
    print(colorama.Fore.GREEN + 'mean: ' + mean_value + ', scale: ' + scale_correction_value + colorama.Style.RESET_ALL)
    result_dict['rpe_of_the_mean_against_gps'] = {
        'mean': float(mean_value),
        'n_trajectories': len(result_dict['successful_runs']),
        'scale_error': 1.0 / float(scale_correction_value)
    }
    result_dict['fraction_of_successful_runs'] = len(list_of_non_diverged_runs) / len(list_of_runs)
    output_dict[main_directory] = result_dict
# 4  Store the summary in a yaml file:
with open('result.yaml', 'w') as outfile:
    yaml.dump(output_dict, outfile)
