sudo sh -c 'echo "deb http://packages.ros.org/ros/ubuntu $(lsb_release -sc) main" > /etc/apt/sources.list.d/ros-latest.list'
sudo apt install -y curl
curl -s https://raw.githubusercontent.com/ros/rosdistro/master/ros.asc | sudo apt-key add -
sudo apt update
sudo apt install -y  ros-noetic-desktop
echo "source /opt/ros/noetic/setup.bash" >> ~/.bashrc
source ~/.bashrc
sudo apt install -y python3-rosdep python3-rosinstall python3-rosinstall-generator python3-wstool build-essential python3-scipy
sudo rosdep init
rosdep update
sudo apt-get install -y python3-catkin-tools libgoogle-glog-dev libgflags-dev libatlas-base-dev ros-noetic-pcl-ros g++-10 ros-noetic-camera-info-manager
sudo update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-10 100 --slave /usr/bin/g++ g++ /usr/bin/g++-10 --slave /usr/bin/gcov gcov /usr/bin/gcov-10
cd ..

git clone https://github.com/mikwaluk/vio-tests.git
cd vio-tests
git submodule update --init --recursive
