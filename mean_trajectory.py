#!/usr/bin/python3
# Partially adapted from https://makersportal.com/blog/2020/4/24/geographic-visualizations-in-python-with-cartopy

import argparse
import os
from typing import Optional

import cartopy.crs as ccrs
import cartopy.io.img_tiles as cimgt
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import utm
import yaml
from cartopy.mpl.ticker import LatitudeFormatter, LongitudeFormatter
from scipy.spatial.transform import Rotation

from cartopy_plot.cartopy_plot import get_map_extent


def get_args() -> argparse.Namespace:
    parser = argparse.ArgumentParser(description='Plots stuff for my thesis')
    parser.add_argument('--dir', type=str, help='Root directory relative to which the files will be searched for')
    parser.add_argument('--config', help='local config yaml filename')
    return parser.parse_args()


def get_timestamp_divisor(timestamp_unit: str) -> float:
    if timestamp_unit == 's':
        return 1.0
    elif timestamp_unit == 'ms':
        return 1000.0
    elif timestamp_unit == 'ns':
        return 1.0e9
    else:
        raise RuntimeError('Odom timestamp units wrong format!')


args = get_args()
directory = os.path.expanduser(args.dir)
with open(os.path.join(directory, args.config), 'r') as stream:
    try:
        plot_config = yaml.safe_load(stream)
    except yaml.YAMLError as exc:
        print(exc)
        exit(1)

# Set default values of certain parametres
gps_color = 'r'
odom_color = 'b'
margin = 0.1
stamen_style = 'toner-lite'
title = 'Reproducibility analysis for ' + plot_config['algorithm_name']
odom_separator = ',' if 'odom_delimitter' not in plot_config else plot_config['odom_delimitter']
list_of_runs = plot_config['folders']

list_of_dfs = []
for folder in list_of_runs:
    odom_file = os.path.join(args.dir, folder, plot_config['odom_file'])
    new_df = pd.read_csv(odom_file, sep=odom_separator)
    # Make the relevant column names uniform so that each has the following form:
    # timestamp x y z qx qy qz
    new_df = new_df.rename(columns={'%time': 'timestamp',
                                    'tx': 'x', 'field.pose.pose.position.x': 'x', 'field.pose.position.x': 'x', 'field.position.x': 'x',
                                    'ty': 'y', 'field.pose.pose.position.y': 'y', 'field.pose.position.y': 'y', 'field.position.y': 'y',
                                    'tz': 'z', 'field.pose.pose.position.z': 'z', 'field.pose.position.z': 'z', 'field.position.z': 'z',
                                    'field.pose.pose.orientation.w': 'qw', 'field.pose.orientation.w': 'qw', 'field.orientation.w': 'qw',
                                    'field.pose.pose.orientation.x': 'qx', 'field.pose.orientation.x': 'qx', 'field.orientation.x': 'qx',
                                    'field.pose.pose.orientation.y': 'qy', 'field.pose.orientation.y': 'qy', 'field.orientation.y': 'qy',
                                    'field.pose.pose.orientation.z': 'qz', 'field.pose.orientation.z': 'qz', 'field.orientation.z': 'qz', })
    list_of_dfs.append(new_df[['timestamp', 'x', 'y', 'z']])

# Because each estimated trajectory has a slightly different length and timestamps,
# to calculate the mean, the missing measurements need to be interpolated
weight = 1/len(list_of_dfs)  # Needed to calculate the mean
mean_df: Optional[pd.DataFrame] = None
for index, df in enumerate(list_of_dfs):
    if index == 0:
        mean_df = df.set_index('timestamp')
        mean_df[['x', 'y', 'z']] *= weight
        continue
    new_df = df.set_index('timestamp')
    new_df[['x', 'y', 'z']] *= weight
    joined = mean_df.join(new_df, rsuffix='_new', how='outer').interpolate()
    new_df_interpolated = joined[['x_new', 'y_new', 'z_new']].rename(columns={'x_new': 'x',
                                                                              'y_new': 'y',
                                                                              'z_new': 'z'})
    mean_df = joined[['x', 'y', 'z']] + new_df_interpolated

mean_df = mean_df.reset_index()
mean_df['x'].iloc[0] = 0.0
mean_df['y'].iloc[0] = 0.0
mean_df['z'].iloc[0] = 0.0
mean_df = mean_df.interpolate()
print('mean_df')
print(mean_df)
gps_file = os.path.join(args.dir, plot_config['gps_file'])
gps_data = pd.read_csv(gps_file)

if plot_config['mean_trajectory_output_file']:
    print(mean_df)
    df_to_print = mean_df.rename(columns={'timestamp': '#timestamp'})[
        ['#timestamp', 'x', 'y', 'z']]
    df_to_print['qx'] = 0.0
    df_to_print['qy'] = 0.0
    df_to_print['qz'] = 0.0
    df_to_print['qw'] = 1.0
    df_to_print['#timestamp'] /= get_timestamp_divisor(plot_config['odom_timestamp_units'])
    print(df_to_print)
    df_to_print.to_csv(plot_config['mean_trajectory_output_file'], sep=' ', index=False)

# Take the first GPS measurement as the origin of the mean trajectory
init_gps_index = 0
init_lat = gps_data['latitude'].iloc[init_gps_index]
init_lng = gps_data['longitude'].iloc[init_gps_index]

# Transform to a local tangential plane (UTM zone)
start_x, start_y, zone_num, zone_letter = utm.from_latlon(init_lat, init_lng)

# Work on a copy to preserve the original dataframe
mean_df_copy = mean_df[['timestamp', 'x', 'y', 'z']].reset_index().copy()

# Read the value of yaw rotation (actually that's a nice use case for the Kabsch-Umeyama algorithm but good for now)
bearing_correction = plot_config['rotation_correction_deg']

rot: Rotation = Rotation.from_euler('XYZ', [0.0, 0.0, bearing_correction], degrees=True)

rotated = rot.apply(mean_df_copy[['x', 'y', 'z']])  # Apply the rotation to align with GPS data
mean_df_copy[['x', 'y', 'z']] = pd.DataFrame(rotated, columns=['x', 'y', 'z'])

# Shift the odom estimate (which starts at cartesian (0,0,0)) to align its origin with the GPS ground truth
mean_df_copy['x'] += start_x
mean_df_copy['y'] += start_y
# After this operation, odom data is in the UTM frame with zone_num and zone_letter


def utm_to_latlon(row: pd.DataFrame): return utm.to_latlon(row['x'], row['y'], zone_num, zone_letter)


# Transform the odom data from UTM to geographic coordinates for easier plotting
mean_df_copy['lat_lon'] = mean_df_copy.apply(lambda row: utm_to_latlon(row), axis=1)

mean_df_copy['latitude'] = mean_df_copy['lat_lon'].apply(lambda row: row[0])
mean_df_copy['longitude'] = mean_df_copy['lat_lon'].apply(lambda row: row[1])

# Combine all trajectories that contributed to the mean so that they are treated as one element of the plot
all_trajectories_lat_long = []
for index, trajectory in enumerate(list_of_dfs):
    rotated = rot.apply(trajectory[['x', 'y', 'z']])  # Apply the initial rotation to align with GPS data
    list_of_dfs[index][['x', 'y', 'z']] = pd.DataFrame(rotated, columns=['x', 'y', 'z'])

    list_of_dfs[index]['x'] += start_x
    list_of_dfs[index]['y'] += start_y
    list_of_dfs[index]['lat_lon'] = trajectory.apply(lambda row: utm_to_latlon(row), axis=1)
    list_of_dfs[index]['latitude'] = trajectory['lat_lon'].apply(lambda row: row[0])
    list_of_dfs[index]['longitude'] = trajectory['lat_lon'].apply(lambda row: row[1])
concatenated = pd.concat(list_of_dfs)

# How much of the map should be shown
extent = get_map_extent(odom_data=concatenated, gps_data=gps_data, margin=margin)

plt.figure(figsize=(12, 9))
map_image = cimgt.Stamen(stamen_style)
ax = plt.axes(projection=map_image.crs)  # Use the proper map projection

ax.set_extent(extent)  # How much of the map should be displayed
ax.set_xticks(np.linspace(extent[0], extent[1], 5), crs=ccrs.PlateCarree())  # Longitude ticks
ax.set_yticks(np.linspace(extent[2], extent[3], 5)[1:], crs=ccrs.PlateCarree())  # Latitude ticks
lon_formatter = LongitudeFormatter(number_format='0.3f', degree_symbol='', dateline_direction_label=True)
lat_formatter = LatitudeFormatter(number_format='0.3f', degree_symbol='')
ax.xaxis.set_major_formatter(lon_formatter)
ax.yaxis.set_major_formatter(lat_formatter)
ax.xaxis.set_tick_params(labelsize=14)
ax.yaxis.set_tick_params(labelsize=14)

# Specify which resolution the background image should have, see source from line 2 for more insights
scale = np.ceil(-np.sqrt(2)*np.log(np.divide((extent[1]-extent[0])/2.0, 350.0)))
scale_max_value = 19  # Scale cannot be larger than 19
if scale > scale_max_value:
    scale = scale_max_value

ax.add_image(map_image, int(scale))

ax.plot(concatenated['longitude'], concatenated['latitude'], markersize=4, marker='.', linestyle='',
        color='lightgreen', transform=ccrs.PlateCarree(), label=f'Trajectory estimates (N={len(list_of_dfs)} runs)')

ax.plot(gps_data['longitude'], gps_data['latitude'], markersize=4, marker='.', linestyle='',
        color=gps_color, transform=ccrs.PlateCarree(), label='GPS signal')
ax.plot(mean_df_copy['longitude'], mean_df_copy['latitude'], markersize=4, marker='.', linestyle='',
        color=odom_color, transform=ccrs.PlateCarree(), label=f'Mean trajectory of N={len(list_of_dfs)} runs')

ax.legend()
filename = plot_config['mean_plot_filename']
ax.set_title(title, fontsize=16)
if not filename:
    plt.show()
else:
    print(f'storing file to {filename}')
    plt.savefig(fname=filename, format='jpg', dpi=400)
    plt.close()
