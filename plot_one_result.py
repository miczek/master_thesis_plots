import argparse
import os

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import yaml

from cartopy_plot.cartopy_plot import plot_gps_and_odom, plot_gps_and_odom_multiple_fragments
from pathlib import Path
import utm


def get_timestamp_divisor(timestamp_unit: str) -> float:
    if timestamp_unit == 's':
        return 1.0
    elif timestamp_unit == 'ms':
        return 1000.0
    elif timestamp_unit == 'ns':
        return 1.0e9
    else:
        raise RuntimeError('Odom timestamp units wrong format!')


def get_args() -> argparse.Namespace:
    parser = argparse.ArgumentParser(description='Plots stuff for my thesis')
    parser.add_argument('--dir', type=str, help='Root directory relative to which the files will be searched for')
    parser.add_argument('--config', default='config.txt', help='Path to local config yaml of the plot')
    return parser.parse_args()


args = get_args()
directory = os.path.expanduser(args.dir)
with open(os.path.join(directory, args.config), 'r') as stream:
    try:
        plot_config = yaml.safe_load(stream)
    except yaml.YAMLError as exc:
        print(exc)
        exit(1)

# Read the data
gps_file = os.path.join(directory, plot_config['gps_filename'])
gps_data = pd.read_csv(gps_file)
gps_timestamp_divisor = get_timestamp_divisor(plot_config['gps_timestamp_units'])
original_gps_timestamp = gps_data['timestamp'].copy() / gps_timestamp_divisor
gps_data['timestamp'] = (gps_data['timestamp'] - gps_data['timestamp'].iloc[0]) / gps_timestamp_divisor

odom_file = os.path.join(directory, plot_config['odom_filename'])
odom_data = pd.read_csv(odom_file, delimiter=plot_config['odom_delimitter'])

# Adapt the column names so that they are consistent for names from various attempts
odom_data = odom_data.rename(columns={'%time': 'timestamp',
                                      'tx': 'x', 'field.pose.pose.position.x': 'x', 'field.pose.position.x': 'x', 'field.position.x': 'x',
                                      'ty': 'y', 'field.pose.pose.position.y': 'y', 'field.pose.position.y': 'y', 'field.position.y': 'y',
                                      'tz': 'z', 'field.pose.pose.position.z': 'z', 'field.pose.position.z': 'z', 'field.position.z': 'z',
                                      'field.pose.pose.orientation.w': 'qw', 'field.pose.orientation.w': 'qw', 'field.orientation.w': 'qw',
                                      'field.pose.pose.orientation.x': 'qx', 'field.pose.orientation.x': 'qx', 'field.orientation.x': 'qx',
                                      'field.pose.pose.orientation.y': 'qy', 'field.pose.orientation.y': 'qy', 'field.orientation.y': 'qy',
                                      'field.pose.pose.orientation.z': 'qz', 'field.pose.orientation.z': 'qz', 'field.orientation.z': 'qz', })
odom_timestamp_divisor = get_timestamp_divisor(plot_config['odom_timestamp_units'])
original_odom_timestamp = odom_data['timestamp'].copy() / odom_timestamp_divisor
odom_data['timestamp'] = (odom_data['timestamp'] - odom_data['timestamp'].iloc[0]) / odom_timestamp_divisor


if 'odom_drop_n_last_frames' in plot_config:
    odom_data.drop(odom_data.tail(plot_config['odom_drop_n_last_frames']).index, inplace=True)

if plot_config['flip_axes']:
    # change to "normal" coordinate frames - x forward, y left, z up
    odom_data['prev_y'] = odom_data['y']
    odom_data['y'] = -odom_data['z']
    odom_data['z'] = odom_data['prev_y']

if plot_config['fragmented_plot']:
    # Fragmented plot means it realigns the odom estimate with the nearest GPS timestamp if odom estimate got interrupted
    plot_gps_and_odom_multiple_fragments(odom_data=odom_data, gps_data=gps_data, title=f'{plot_config["algorithm_name"]} vs GPS',
                                         plot_dir=args.dir, map_config=plot_config, max_interval=plot_config['max_allowed_interval'],
                                         stamen_style='toner-lite')
else:
    correction = plot_config['subpath_correction']
    rotation = correction[0][1]
    x_translation = correction[0][2]
    y_translation = correction[0][3]
    plot_gps_and_odom(odom_data=odom_data, gps_data=gps_data, title=f'{plot_config["algorithm_name"]} vs GPS', filename=os.path.join(args.dir, 'map_result.jpg'),
                      rotation_deg=rotation, x_offset=x_translation, y_offset=y_translation,
                      stamen_style='toner-lite')
# Read only every nth entry for derivations, otherwise the data would be too noisy
odom_data_copy = odom_data.copy()[odom_data.index % plot_config['odom_speed_sampling'] == 0]
# Only if GPS provides the speed estimate
if 'speed' in gps_data:
    if plot_config['odom_use_provided_speed']:  # If odom provides speed data, no need to calculate position derivatives
        odom_data_copy['speed'] = np.sqrt(odom_data_copy.vx**2 + odom_data_copy.vy**2 + odom_data_copy.vz**2)
    else:  # v = ds/dt
        dist = odom_data_copy[['timestamp', 'x', 'y', 'z']].copy().diff().fillna(0.)
        dist['Dist'] = np.sqrt(dist.x**2 + dist.y**2 + dist.z**2)
        dist['Speed'] = dist.Dist / (dist.timestamp)
        odom_data_copy['speed'] = dist.Speed

    odom_time_series = pd.Series(odom_data_copy.speed)
    odom_time_series.index = odom_data_copy.timestamp
    gps_time_series = pd.Series(gps_data.speed)
    gps_time_series.index = gps_data.timestamp
    interpolated_df = pd.DataFrame({'gps_speed': gps_time_series, 'odom_speed': odom_time_series})
    interpolated_df = interpolated_df.interpolate('index')

    id_name = os.path.splitext(os.path.basename(odom_file))[0]

    plt.title(f'Speed comparison for {plot_config["algorithm_name"]}')
    plt.plot(interpolated_df.index, interpolated_df.odom_speed, label='estimated speed')
    plt.plot(interpolated_df.index, interpolated_df.gps_speed, label='gps speed')
    plt.plot(interpolated_df.index, interpolated_df.odom_speed -
             interpolated_df.gps_speed, '--', label='difference (odom - gps)')
    plt.xlabel('time [s]')
    plt.ylabel('speed [m/s]')
    plt.legend()
    plt.savefig(fname=os.path.join(directory, f'gps_vs_odom_speed_difference.jpg'), format='jpg')
    plt.close()

if 'resource_usage_filename' in plot_config:
    print('resource usage')
    max_cpu = plot_config['resource_usage_max_cpu']
    resource_usage = pd.read_csv(os.path.join(args.dir, plot_config['resource_usage_filename']))
    resource_usage = resource_usage.rename(columns={'cpu': 'cpu_percent', 'mem': 'memory_percent'})
    timestamp_divisor = get_timestamp_divisor(plot_config['resource_usage_timestamp_units'])
    resource_usage['timestamp'] = (resource_usage['timestamp'] -
                                   resource_usage['timestamp'].iloc[0]) / timestamp_divisor
    fig, ax = plt.subplots(2)
    ax[0].plot(resource_usage['timestamp'], resource_usage['cpu_percent'], label='Usage of 1 CPU')
    ax[1].plot(resource_usage['timestamp'], resource_usage['memory_percent'], label='Memory')
    ax[0].set_title('Usage of resources')
    ax[0].set_ylabel(f'% of one CPU (max {max_cpu})')
    ax[1].set_ylabel('% of RAM')
    ax[1].set_xlabel('time [s]')
    fig.savefig(os.path.join(args.dir, 'resource_usage.jpg'), format='jpg')
    plt.clf()

ground_truth_file = os.path.join(args.dir, 'global_pose.csv')

if 'generate_tum_format_from_odom' in plot_config and plot_config['generate_tum_format_from_odom']:
    required_columns = ['timestamp', 'x', 'y', 'z']
    unwanted_columns = [column for column in odom_data.columns if column not in required_columns]
    odom_data_copy = odom_data.copy().drop(columns=unwanted_columns)

    # Check if all required columns are there
    for desired_column in required_columns:
        if desired_column not in odom_data.columns:
            raise RuntimeError(
                f'Could not find all required columns in the odom file! Missing column: {desired_column}')

    # Reorder the columns
    odom_data_copy = odom_data_copy[required_columns]

    new_file_name = Path(os.path.basename(odom_file)).stem + '_tum_format.txt'
    new_file_path = os.path.join(os.path.dirname(odom_file), new_file_name)

    # Make the header line appear as a comment in the tum format
    odometry_dataframe = odom_data_copy.rename(columns={'timestamp': '#timestamp'})
    odometry_dataframe['#timestamp'] = original_odom_timestamp
    odometry_dataframe['qx'] = 0.0
    odometry_dataframe['qy'] = 0.0
    odometry_dataframe['qz'] = 0.0
    odometry_dataframe['qw'] = 1.0
    odometry_dataframe.to_csv(path_or_buf=new_file_path, sep=' ', index=False)


required_columns = ['timestamp', 'latitude', 'longitude', 'altitude']
unwanted_columns = [column for column in gps_data.columns if column not in required_columns]
gps_data_copy = gps_data.copy().drop(columns=unwanted_columns)


# Check if all required columns are there
for desired_column in required_columns:
    if desired_column not in gps_data_copy.columns:
        raise RuntimeError(
            f'Could not find all required columns in the odom file! Missing column: {desired_column}')
# start_x, start_y, zone_num, zone_letter = utm.from_latlon(init_lat, init_lng)
def latlon_to_utm(row: pd.DataFrame): return utm.from_latlon(row['latitude'], row['longitude'])


gps_data_copy['utm_conversion_result'] = gps_data_copy.apply(lambda row: latlon_to_utm(row), axis=1)

gps_data_copy['x'] = gps_data_copy['utm_conversion_result'].apply(lambda row: row[0])
gps_data_copy['y'] = gps_data_copy['utm_conversion_result'].apply(lambda row: row[1])
gps_data_copy['z'] = gps_data_copy['altitude']

gps_data_copy[['x', 'y', 'z']] -= gps_data_copy[['x', 'y', 'z']].iloc[0]
# Reorder the columns
unwanted_columns = [column for column in gps_data_copy.columns if column not in ['timestamp', 'x', 'y', 'z']]
gps_data_copy = gps_data_copy.drop(columns=unwanted_columns)
gps_data_copy['qx'] = 0.0
gps_data_copy['qy'] = 0.0
gps_data_copy['qz'] = 0.0
gps_data_copy['qw'] = 1.0

first = odom_data_copy.iloc[0]
last = odom_data_copy.iloc[-1]
dist = np.sqrt((last.x - first.x)**2 + (last.y - first.y)**2 + (last.z - first.z)**2)
print(f'{os.path.basename(directory)}: Difference from start to end: {dist}')
if 'generate_tum_format_from_gps' in plot_config and plot_config['generate_tum_format_from_gps']:
    gps_data_copy = gps_data_copy[['timestamp', 'x', 'y', 'z', 'qx', 'qy', 'qz', 'qw']]
    gps_data_copy['timestamp'] = original_gps_timestamp
    new_file_name = Path(os.path.basename(gps_file)).stem + '_tum_format.txt'
    new_file_path = os.path.join(os.path.dirname(gps_file), new_file_name)
    # Make the header line appear as a comment in the tum format
    gps_dataframe = gps_data_copy.rename(columns={'timestamp': '#timestamp'})
    gps_dataframe.to_csv(path_or_buf=new_file_path, sep=' ', index=False)
