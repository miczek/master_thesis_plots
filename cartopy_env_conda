name: cartopy_env
channels:
  - anaconda
  - conda-forge
  - defaults
dependencies:
  - _libgcc_mutex=0.1=conda_forge
  - _openmp_mutex=4.5=1_gnu
  - autopep8=1.5.7=pyhd3eb1b0_0
  - blas=1.0=openblas
  - bottleneck=1.3.2=py39hdd57654_1
  - c-ares=1.17.2=h7f98852_0
  - ca-certificates=2021.7.5=h06a4308_1
  - cartopy=0.19.0.post1=py39h3b23250_0
  - certifi=2021.5.30=py39h06a4308_0
  - cycler=0.10.0=py_2
  - dbus=1.13.18=hb2f20db_0
  - expat=2.4.1=h2531618_2
  - fontconfig=2.13.1=h6c09931_0
  - freetype=2.10.4=h0708190_1
  - geos=3.9.1=h9c3ff4c_2
  - glib=2.69.1=h5202010_0
  - gst-plugins-base=1.14.0=h8213a91_2
  - gstreamer=1.14.0=h28cd5cc_2
  - icu=58.2=he6710b0_3
  - jbig=2.1=h7f98852_2003
  - jpeg=9d=h36c2ea0_0
  - kiwisolver=1.3.1=py39h1a9c180_1
  - krb5=1.19.2=hcc1bbae_0
  - lcms2=2.12=hddcbb42_0
  - ld_impl_linux-64=2.36.1=hea4e1c9_2
  - lerc=2.2.1=h9c3ff4c_0
  - libblas=3.9.0=11_linux64_openblas
  - libcblas=3.9.0=11_linux64_openblas
  - libcurl=7.78.0=h2574ce0_0
  - libdeflate=1.7=h7f98852_5
  - libedit=3.1.20191231=he28a2e2_2
  - libev=4.33=h516909a_1
  - libffi=3.3=h58526e2_2
  - libgcc-ng=11.1.0=hc902ee8_8
  - libgfortran-ng=11.1.0=h69a702a_8
  - libgfortran5=11.1.0=h6c583b3_8
  - libgomp=11.1.0=hc902ee8_8
  - liblapack=3.9.0=11_linux64_openblas
  - libnghttp2=1.43.0=h812cca2_0
  - libopenblas=0.3.17=pthreads_h8fe5266_1
  - libpng=1.6.37=h21135ba_2
  - libssh2=1.9.0=ha56f1ee_6
  - libstdcxx-ng=11.1.0=h56837e0_8
  - libtiff=4.3.0=hf544144_1
  - libuuid=1.0.3=h1bed415_2
  - libwebp-base=1.2.0=h7f98852_2
  - libxcb=1.14=h7b6447c_0
  - libxml2=2.9.12=h03d6c58_0
  - lz4-c=1.9.3=h9c3ff4c_1
  - matplotlib=3.4.2=py39h06a4308_0
  - matplotlib-base=3.4.2=py39h2fa2bec_0
  - mypy=0.910=pyhd3eb1b0_0
  - mypy_extensions=0.4.3=py39h06a4308_0
  - ncurses=6.2=h58526e2_4
  - numexpr=2.7.3=py39h4be448d_1
  - numpy=1.21.1=py39hdbf815f_0
  - olefile=0.46=pyh9f0ad1d_1
  - openjpeg=2.4.0=hb52868f_1
  - openssl=1.1.1k=h27cfd23_0
  - pandas=1.3.1=py39h8c16a72_0
  - pcre=8.45=h295c915_0
  - pillow=8.3.1=py39ha612740_0
  - pip=21.2.3=pyhd8ed1ab_0
  - proj=7.2.0=h277dcde_2
  - psutil=5.8.0=py39h27cfd23_1
  - pycodestyle=2.7.0=pyhd3eb1b0_0
  - pyparsing=2.4.7=pyh9f0ad1d_0
  - pyqt=5.9.2=py39h2531618_6
  - pyshp=2.1.3=pyh44b312d_0
  - python=3.9.6=h49503c6_1_cpython
  - python-dateutil=2.8.2=pyhd8ed1ab_0
  - python_abi=3.9=2_cp39
  - pytz=2021.1=pyhd3eb1b0_0
  - pyyaml=5.4.1=py39h27cfd23_1
  - qt=5.9.7=h5867ecd_1
  - readline=8.1=h46c0cb4_0
  - scipy=1.7.1=py39hee8e79c_0
  - seaborn=0.11.2=pyhd3eb1b0_0
  - setuptools=49.6.0=py39hf3d152e_3
  - shapely=1.7.1=py39ha61afbd_5
  - sip=4.19.13=py39h2531618_0
  - six=1.16.0=pyh6c4a22f_0
  - sqlite=3.36.0=h9cd32fc_0
  - tk=8.6.10=h21135ba_1
  - toml=0.10.2=pyhd3eb1b0_0
  - tornado=6.1=py39h3811e60_1
  - typing_extensions=3.10.0.0=pyh06a4308_0
  - tzdata=2021a=he74cb21_1
  - wheel=0.37.0=pyhd8ed1ab_0
  - xz=5.2.5=h516909a_1
  - yaml=0.2.5=h7b6447c_0
  - zlib=1.2.11=h516909a_1010
  - zstd=1.5.0=ha95c52a_0
  - pip:
    - utm==0.7.0
prefix: /home/mikolaj/anaconda3/envs/cartopy_env
