import re
import pandas as pd
import matplotlib.pyplot as plt
import os


timing_file_path = 'results/openVins/kaist_urban_39/run_1/traj_timing.txt'

timing_df = pd.read_csv(timing_file_path)
print(timing_df)

feature_tracker_series = timing_df.tracking * 1000
calculation_times_series = (timing_df.total - timing_df.tracking) * 1000

plot = feature_tracker_series.hist()
plot.set_title('Distribution of feature tracker timing for OpenVINS')
plot.set_xlabel('Processing times [ms]')
plot.set_ylabel('number of samples')
plt.plot([], [], ' ', label=f'Mean = {feature_tracker_series.mean():.2f} ms')
plt.plot([], [], ' ', label=f'Median = {feature_tracker_series.median():.2f} ms')
plt.legend()
for bar in plot.patches:
    height = bar.get_height()
    plot.annotate(f'{int(height)}', xy=(bar.get_x()+bar.get_width()/2, height),
                  xytext=(0, 5), textcoords='offset points', ha='center', va='bottom')
plot.set_ylim(plot.get_ylim()[0], plot.get_ylim()[1]*1.05)
plt.show()

plot = calculation_times_series.hist()
plot.set_title('Distribution of calculations timing for OpenVINS')
plot.set_xlabel('Processing times [ms]')
plot.set_ylabel('number of samples')
plt.plot([], [], ' ', label=f'Mean = {calculation_times_series.mean():.2f} ms')
plt.plot([], [], ' ', label=f'Median = {calculation_times_series.median():.2f} ms')
plt.legend()
for bar in plot.patches:
    height = bar.get_height()
    plot.annotate(f'{int(height)}', xy=(bar.get_x()+bar.get_width()/2, height),
                  xytext=(0, 5), textcoords='offset points', ha='center', va='bottom')
plot.set_ylim(plot.get_ylim()[0], plot.get_ylim()[1]*1.05)
print(f'{calculation_times_series.std()=}')
print(f'{calculation_times_series.max()=}')
plt.show()
