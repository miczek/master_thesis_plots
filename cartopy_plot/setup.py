from setuptools import setup

setup(name='cartopy_plot',
      version='0.1',
      description='Facilitate plotting with CartoPy',
      url='http://github.com/storborg/funniest',
      author='Mikolaj Walukiewicz',
      author_email='mikolaj@peregrine.ai',
      license='MIT',
      packages=['cartopy_plot'],
      zip_safe=False)
