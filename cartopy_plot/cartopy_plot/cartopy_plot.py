# Partially adapted from https://makersportal.com/blog/2020/4/24/geographic-visualizations-in-python-with-cartopy

import os
from typing import List, Optional

import cartopy.crs as ccrs
import cartopy.io.img_tiles as cimgt
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import utm
from cartopy.mpl.ticker import LatitudeFormatter, LongitudeFormatter
from scipy.spatial.transform import Rotation

# How much of the map should be shown
# Returns two pairs of coordinates (min/max latitude and longitude) between which the map should be displayed


def get_map_extent(odom_data: pd.DataFrame, gps_data: pd.DataFrame, margin: float) -> List[float]:
    if 'longitude' in odom_data:
        min_lon = min(gps_data['longitude'].min(), odom_data['longitude'].min())
        max_lon = max(gps_data['longitude'].max(), odom_data['longitude'].max())
        min_lat = min(gps_data['latitude'].min(), odom_data['latitude'].min())
        max_lat = max(gps_data['latitude'].max(), odom_data['latitude'].max())
    else:
        min_lon = gps_data['longitude'].min()
        max_lon = gps_data['longitude'].max()
        min_lat = gps_data['latitude'].min()
        max_lat = gps_data['latitude'].max()
    lon_diff = max_lon - min_lon
    lat_diff = max_lat - min_lat

    abs_margin = min(lat_diff, lon_diff) * margin

    map_min_lon = min_lon - abs_margin
    map_max_lon = max_lon + abs_margin
    map_min_lat = min_lat - abs_margin
    map_max_lat = max_lat + abs_margin

    extent = [map_min_lon, map_max_lon, map_min_lat, map_max_lat]
    return extent


def plot_gps_and_odom(odom_data: pd.DataFrame, gps_data: pd.DataFrame, title: str, filename: Optional[str] = None,
                      gps_color: str = '#b30909', odom_color: str = '#045a8d',
                      stamen_style: str = 'terrain', format: str = 'jpg', margin: float = 0.1,
                      rotation_deg: float = 0.0, x_offset: float = 0.0, y_offset: float = 0.0) -> None:
    """Stores a scatter plot of gps and odom measurements into a file or displays it on the screen

    Args:
        odom_data (pd.DataFrame): The frame must contain columns named ['timestamp', 'x', 'y', 'z']
        gps_data (pd.DataFrame): The frame must contain columns named ['timestamp', 'latitude', 'longitude']
        title (str): Title of the plot
        filename (Optional[str]): Where to store the generated plot; if None, display the plot instead
        gps_color (str): Color of the GPS markers
        odom_color (str): Color of the odometry markers
        stamen_style (str): Style of the map in the background. Recommended values: 'terrain' or 'toner-lite'
        format (str): Image format to store the plot
        margin (float): How much free space between the plot border and outest marker should be left (0-1)
        rotation_deg (float): Degrees by which odom estimate should be rotated around the z axis (yaw) to align with GPS
        x_offset (float): Meters by which the odom estimate should be shifted in positive x direction to align with GPS
        y_offset (float): Meters by which the odom estimate should be shifted in positive y direction to align with GPS
    Returns:
        None
    """

    # Align the odom data with the first GPS timestamp that came after the first odom measurement
    init_gps_index = gps_data[gps_data['timestamp'].gt(odom_data['timestamp'].iloc[0])].index[0]
    init_lat = gps_data['latitude'].iloc[init_gps_index]
    init_lng = gps_data['longitude'].iloc[init_gps_index]

    # Transform to a local tangential plane (UTM zone)
    start_x, start_y, zone_num, zone_letter = utm.from_latlon(init_lat, init_lng)

    # Work on a copy to preserve the original dataframe
    odom_data_copy = odom_data[['timestamp', 'x', 'y', 'z']].reset_index().copy()

    bearing_correction = rotation_deg

    rot: Rotation = Rotation.from_euler('XYZ', [0.0, 0.0, bearing_correction], degrees=True)

    rotated = rot.apply(odom_data_copy[['x', 'y', 'z']])  # Apply the initial rotation to align with GPS data
    odom_data_copy[['x', 'y', 'z']] = pd.DataFrame(rotated, columns=['x', 'y', 'z'])

    # x_offset: apply the offset to compensate for wrong GPS readings; positive means go right (to the east)
    # y_offset: positive means go up (to the north)

    odom_data_copy['x'] += start_x + x_offset
    odom_data_copy['y'] += start_y + y_offset

    def utm_to_latlon(row: pd.DataFrame): return utm.to_latlon(row['x'], row['y'], zone_num, zone_letter)
    odom_data_copy['lat_lon'] = odom_data_copy.apply(lambda row: utm_to_latlon(row), axis=1)

    odom_data_copy['latitude'] = odom_data_copy['lat_lon'].apply(lambda row: row[0])
    odom_data_copy['longitude'] = odom_data_copy['lat_lon'].apply(lambda row: row[1])

    extent = get_map_extent(odom_data=odom_data_copy, gps_data=gps_data, margin=margin)

    plt.figure(figsize=(12, 9))
    map_image = cimgt.Stamen(stamen_style)
    ax = plt.axes(projection=map_image.crs)  # Use the proper map projection

    ax.set_extent(extent)  # How much of the map should be displayed
    ax.set_xticks(np.linspace(extent[0], extent[1], 5), crs=ccrs.PlateCarree())  # Longitude ticks
    ax.set_yticks(np.linspace(extent[2], extent[3], 5)[1:], crs=ccrs.PlateCarree())  # Latitude ticks
    lon_formatter = LongitudeFormatter(number_format='0.3f', degree_symbol='', dateline_direction_label=True)
    lat_formatter = LatitudeFormatter(number_format='0.3f', degree_symbol='')
    ax.xaxis.set_major_formatter(lon_formatter)
    ax.yaxis.set_major_formatter(lat_formatter)
    ax.xaxis.set_tick_params(labelsize=14)
    ax.yaxis.set_tick_params(labelsize=14)

    # Some magic, see source in line 1
    scale = np.ceil(-np.sqrt(2)*np.log(np.divide((extent[1]-extent[0])/2.0, 350.0)))  # + 1
    scale_max_value = 19  # Scale cannot be larger than 19
    if scale > scale_max_value:
        scale = scale_max_value

    ax.add_image(map_image, int(scale))  # add the background image we previously downloaded

    ax.plot(gps_data['longitude'], gps_data['latitude'], markersize=4, marker='.', linestyle='',
            color=gps_color, transform=ccrs.PlateCarree(), label='GPS signal')
    ax.plot(odom_data_copy['longitude'], odom_data_copy['latitude'], markersize=4, marker='.', linestyle='',
            color=odom_color, transform=ccrs.PlateCarree(), label='Odom estimate')

    ax.legend()
    ax.set_title(title, fontsize=16)
    if filename is None:
        plt.show()
    else:
        print(f'storing file to {filename}')
        plt.savefig(fname=filename, format=format, dpi=400)
        plt.close()


def plot_gps_and_odom_multiple_fragments(odom_data: pd.DataFrame, gps_data: pd.DataFrame, title: str, plot_dir: str, map_config: dict,
                                         gps_color='#b30909', odom_color='#045a8d',
                                         stamen_style='terrain', format='jpg', margin=0.1,
                                         max_interval=10.0) -> None:
    """Stores a scatter plot of gps and odom measurements into a file, aligning fragments of the plot with GPS signal

    Args:
        odom_data (pd.DataFrame): The frame must contain columns named ['timestamp', 'x', 'y', 'z']
        gps_data (pd.DataFrame): The frame must contain columns named ['timestamp', 'latitude', 'longitude']
        title (str): Title of the plot
        plot_dir (str): Where to store the generated plot
        map_config (dict): Dictionary with plot configuration (e.g. odom data translation/rotation for the specific fragments)
        gps_color (str): Color of the GPS markers
        odom_color (str): Color of the odometry markers
        stamen_style (str): Style of the map in the background. Recommended values: 'terrain' or 'toner-lite'
        format (str): Image format to store the plot
        margin (float): How much free space between the plot border and outest marker should be left (0-1)
        max_interval (float): Max allowed time difference between two consecutive odom estimates in seconds.
                              If Exceeded, the upcoming data will be plotted in a new fragment, realigned with the nearest GPS timestamp.
    Returns:
        None
    """

    # Align the odom data with the first GPS timestamp that came after the first odom measurement
    print(odom_data["timestamp"].shift() + max_interval)
    ids = (odom_data["timestamp"] > (odom_data["timestamp"].shift() + max_interval)).cumsum()
    grouped = odom_data.groupby(ids)

    # initialize the corrections with zeroes, overwrite only entries with specified indices
    x_y_array = [(0.0, 0.0)] * len(grouped)
    angle_array = [0.0] * len(grouped)
    for index, alpha, x, y in map_config['subpath_correction']:
        x_y_array[index] = (x, y)
        angle_array[index] = alpha

    plt.figure(figsize=(12, 9))
    map_image = cimgt.Stamen(stamen_style)
    ax = plt.axes(projection=map_image.crs)  # Use the proper map projection
    extent = get_map_extent(odom_data=odom_data, gps_data=gps_data, margin=margin)
    ax.set_extent(extent)  # How much of the map should be displayed
    # Some empirical magic, see source in line 1
    scale = np.ceil(-np.sqrt(2)*np.log(np.divide((extent[1]-extent[0])/2.0, 350.0)))
    scale_max_value = 19  # Scale cannot be larger than 19
    if scale > scale_max_value:
        scale = scale_max_value

    ax.add_image(map_image, int(scale))  # add the background image we previously downloaded
    df_to_plot = pd.DataFrame(columns=['timestamp', 'latitude', 'longitude'])
    for index, group in grouped:
        print(index)
        group['x'] -= group['x'].iloc[0]
        group['y'] -= group['y'].iloc[0]
        group['z'] -= group['z'].iloc[0]

        init_gps_index = gps_data[gps_data['timestamp'].gt(group['timestamp'].iloc[0])].index[0]
        init_lat = gps_data['latitude'].iloc[init_gps_index]
        init_lng = gps_data['longitude'].iloc[init_gps_index]

        start_x, start_y, zone_num, zone_letter = utm.from_latlon(init_lat, init_lng)

        odom_data_copy = group[['timestamp', 'x', 'y', 'z']].reset_index().copy()
        bearing_correction = angle_array[index]

        rot: Rotation = Rotation.from_euler('XYZ', [0.0, 0.0, bearing_correction], degrees=True)

        rotated = rot.apply(odom_data_copy[['x', 'y', 'z']])  # Apply the initial rotation to align with GPS data
        odom_data_copy[['x', 'y', 'z']] = pd.DataFrame(rotated, columns=['x', 'y', 'z'])

        # Apply the offset to compensate for wrong GPS readings;
        x_offset = x_y_array[index][0]  # Positive means go right (to the east)
        y_offset = x_y_array[index][1]  # Positive means go up (to the north)

        odom_data_copy['x'] += start_x + x_offset
        odom_data_copy['y'] += start_y + y_offset

        def utm_to_latlon(row: pd.DataFrame): return utm.to_latlon(row['x'], row['y'], zone_num, zone_letter)
        odom_data_copy['lat_lon'] = odom_data_copy.apply(lambda row: utm_to_latlon(row), axis=1)

        odom_data_copy['latitude'] = odom_data_copy['lat_lon'].apply(lambda row: row[0])
        odom_data_copy['longitude'] = odom_data_copy['lat_lon'].apply(lambda row: row[1])
        df_to_plot = df_to_plot.append(odom_data_copy)
    print(df_to_plot)
    ax.plot(df_to_plot['longitude'], df_to_plot['latitude'], markersize=4, marker='.', linestyle='',
            color=odom_color, transform=ccrs.PlateCarree(), label='Odom estimate')

    # Set plot/axis format and style
    ax.set_xticks(np.linspace(extent[0], extent[1], 5), crs=ccrs.PlateCarree())  # Longitude ticks
    ax.set_yticks(np.linspace(extent[2], extent[3], 5)[1:], crs=ccrs.PlateCarree())  # Latitude ticks
    lon_formatter = LongitudeFormatter(number_format='0.3f', degree_symbol='', dateline_direction_label=True)
    lat_formatter = LatitudeFormatter(number_format='0.3f', degree_symbol='')
    ax.xaxis.set_major_formatter(lon_formatter)
    ax.yaxis.set_major_formatter(lat_formatter)
    ax.xaxis.set_tick_params(labelsize=14)
    ax.yaxis.set_tick_params(labelsize=14)

    ax.plot(gps_data['longitude'], gps_data['latitude'], markersize=4, marker='.', linestyle='',
            color=gps_color, transform=ccrs.PlateCarree(), label='GPS signal')

    ax.legend()
    ax.set_title(title, fontsize=16)
    plt.savefig(fname=os.path.join(plot_dir, f'odom_vs_gps_cartopy_bobo.{format}'), format=format, dpi=400)
    plt.close()
