import re
import pandas as pd
import matplotlib.pyplot as plt
import os


timing_file_path = '/home/mikolaj/Projects/plot_results/results/ARCore/spaziergang_leon/odom_result.csv'

timing_df = pd.read_csv(timing_file_path)

diff_timing_series = timing_df.timestamp.diff()
print(diff_timing_series)
plot = diff_timing_series.hist()
plot.set_title('Distribution of processing times for Google ARCore')
plot.set_xlabel('Processing times [ms]')
plot.set_ylabel('number of samples')
#plt.xlim([0, 80])
plt.plot([], [], ' ', label=f'Mean = {diff_timing_series.mean():.2f} ms')
plt.plot([], [], ' ', label=f'Median = {diff_timing_series.median():.2f} ms')
plt.legend()
for bar in plot.patches:
    height = bar.get_height()
    plot.annotate(f'{int(height)}', xy=(bar.get_x()+bar.get_width()/2, height),
                  xytext=(0, 5), textcoords='offset points', ha='center', va='bottom')
plot.set_ylim(plot.get_ylim()[0], plot.get_ylim()[1]*1.05)
print(f'{diff_timing_series.max()}')
print(f'{diff_timing_series.std()}')
plt.show()
