import pandas as pd
import matplotlib.pyplot as plt

max_cpu = 800

resource_usage_vins_mono = pd.read_csv('results/vins-mobile/two_loops_no_segfault/resource_usage.csv')
resource_usage_arcore = pd.read_csv('results/ARCore/prep_test_drive/resource_usage.csv')
resource_usage_vins_mono = resource_usage_vins_mono.rename(columns={'cpu': 'cpu_percent', 'mem': 'memory_percent'})
resource_usage_arcore = resource_usage_arcore.rename(columns={'cpu': 'cpu_percent', 'mem': 'memory_percent'})
timestamp_divisor = 1000.0
resource_usage_vins_mono['timestamp'] = (resource_usage_vins_mono['timestamp'] -
                                         resource_usage_vins_mono['timestamp'].iloc[0]) / timestamp_divisor
resource_usage_arcore['timestamp'] = (resource_usage_arcore['timestamp'] -
                                      resource_usage_arcore['timestamp'].iloc[0]) / timestamp_divisor
fig, ax = plt.subplots(2)
ax[0].plot(resource_usage_vins_mono['timestamp'], resource_usage_vins_mono['cpu_percent'], label='VINS-Mobile')
ax[1].plot(resource_usage_vins_mono['timestamp'],
           resource_usage_vins_mono['memory_percent'] * 1024 * 6 / 100, label='VINS-Mobile')
ax[0].plot(resource_usage_arcore['timestamp'], resource_usage_arcore['cpu_percent'], label='ARCore')
ax[1].plot(resource_usage_arcore['timestamp'],
           resource_usage_arcore['memory_percent'] * 1024 * 6 / 100, label='ARCore')
ax[0].set_title('Usage of resources')
ax[0].set_ylabel(f'% of one CPU (max {max_cpu})')
ax[1].set_ylabel('RAM usage [MB]')
ax[1].set_xlabel('time [s]')
ax[0].set_xlim([0, 260])
ax[1].set_xlim([0, 260])
plt.legend()
plt.show()
#fig.savefig('resource_usage.jpg', format='jpg')
