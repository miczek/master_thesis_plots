import argparse
import os
import subprocess
import yaml

import colorama


def get_args() -> argparse.Namespace:
    parser = argparse.ArgumentParser(description='Automated execution of the plotting script')
    parser.add_argument('config', help='local config yaml filename')
    parser.add_argument('-f', '--force', action='store_true', help='also overwrite existing plots')
    return parser.parse_args()


args = get_args()
with open(args.config, 'r') as stream:
    try:
        config = yaml.safe_load(stream)
    except yaml.YAMLError as exc:
        print(exc)
        exit(1)

for main_directory in config['list_of_directories']:
    directory_content = os.listdir(main_directory)
    list_of_dirs_with_plots = []
    for elem in directory_content:
        if config['result_folder_prefix'] in elem:
            list_of_dirs_with_plots.append(elem)
    list_of_dirs_with_plots.sort()
    for result_dir_name in list_of_dirs_with_plots:
        result_dir_path = os.path.join(main_directory, result_dir_name)
        result_dir_content = os.listdir(result_dir_path)
        file_extension_found = False
        for elem in result_dir_content:
            if config['desired_plot_format'] in elem:
                file_extension_found = True
        if args.force or not file_extension_found:
            plot_cmd = (f'python3 {config["plot_script_path"]} --dir {result_dir_path}'
                        f' --config {config["plot_config_filename"]}')
            need_to_work = True
            while need_to_work:
                print(colorama.Fore.YELLOW + plot_cmd + colorama.Style.RESET_ALL)
                process = subprocess.run(['/bin/bash', '-c', plot_cmd], capture_output=True)
                try:
                    output = process.stdout.decode('utf-8')
                    if 'error' in output or process.returncode != 0:
                        print(colorama.Fore.RED + process.stderr.decode('utf-8') + colorama.Style.RESET_ALL)
                        exit(1)
                    else:
                        need_to_work = False
                except UnicodeDecodeError as error:
                    print(error)
