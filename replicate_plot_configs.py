import argparse
import os
import re
from shutil import copyfile
import yaml


def get_args() -> argparse.Namespace:
    parser = argparse.ArgumentParser(description='Copies plot configuration')
    parser.add_argument('config', help='local config yaml filename')
    parser.add_argument('--force', action='store_true', help='also overwrite existing files')
    return parser.parse_args()


args = get_args()
with open(args.config, 'r') as stream:
    try:
        config_list = yaml.safe_load(stream)
    except yaml.YAMLError as exc:
        print(exc)
        exit(1)

for config in config_list:
    result_path = config['result_path']
    file_to_copy_full_path = config['plot_config_to_replicate']
    file_to_copy_name = os.path.basename(file_to_copy_full_path)
    result_folder_pattern = re.compile(f'^{config["result_folder_prefix"]}(\d+)$')
    ls_result = os.listdir(result_path)
    folders_with_results = []
    for elem in ls_result:
        match = result_folder_pattern.match(elem)
        if os.path.isdir(os.path.join(config['result_path'], elem)) and match:
            folders_with_results.append(elem)
    for folder_name in folders_with_results:
        folder_content = os.listdir(os.path.join(result_path, folder_name))
        if args.force or file_to_copy_name not in folder_content:
            print(f'copying from {file_to_copy_full_path} to {os.path.join(result_path, folder_name, file_to_copy_name)}')
            copyfile(src=file_to_copy_full_path, dst=os.path.join(result_path, folder_name, file_to_copy_name))
