import re
import pandas as pd
import matplotlib.pyplot as plt
import os
from typing import Optional


def parse_line(string: str) -> re.match:
    return re.search('\s*Filter Update:\s+(?P<time>\d+.\d+)\s+ms for processing (?P<n>\d+) images.*', string)


log_file = os.path.realpath(os.path.expanduser('results/rovio/szczecin_small_loop/rosout_timing.log'))


just_imu = []
with_image = []
with open(log_file, 'r') as file:
    print(f'reading {file}')
    lines = file.readlines()

for line in lines:
    if match := parse_line(line):
        if int(match.group('n')):
            with_image.append(float(match.group('time')))
        else:
            just_imu.append(float(match.group('time')))


just_imu_series = pd.Series(just_imu)
with_image_series = pd.Series(with_image)
plot = just_imu_series.hist()
print(f'{just_imu_series.std()=}')
plot.set_title('Distribution of IMU preintegration timing for ROVIO')
plot.set_xlabel('Processing times [ms]')
plot.set_ylabel('number of samples')
plt.plot([], [], ' ', label=f'Mean = {just_imu_series.mean():.4f} ms')
plt.plot([], [], ' ', label=f'Median = {just_imu_series.median():.4f} ms')
plt.legend()
for bar in plot.patches:
    height = bar.get_height()
    plot.annotate(f'{int(height)}', xy=(bar.get_x()+bar.get_width()/2, height),
                  xytext=(0, 5), textcoords='offset points', ha='center', va='bottom')
plot.set_ylim(plot.get_ylim()[0], plot.get_ylim()[1]*1.05)
plt.show()

plot = with_image_series.hist()
plot.set_title('Distribution of image update timing for ROVIO')
print(f'{with_image_series.std()=}')
plot.set_xlabel('Processing times [ms]')
plot.set_ylabel('number of samples')
plt.plot([], [], ' ', label=f'Mean = {with_image_series.mean():.2f} ms')
plt.plot([], [], ' ', label=f'Median = {with_image_series.median():.2f} ms')
plt.legend()
for bar in plot.patches:
    height = bar.get_height()
    plot.annotate(f'{int(height)}', xy=(bar.get_x()+bar.get_width()/2, height),
                  xytext=(0, 5), textcoords='offset points', ha='center', va='bottom')
plot.set_ylim(plot.get_ylim()[0], plot.get_ylim()[1]*1.05)
print(f'{with_image_series.std()=}')
print(f'{with_image_series.max()=}')
plt.show()
