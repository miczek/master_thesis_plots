import argparse
import copy
from datetime import datetime
import os
import re
import signal
import subprocess
import sys
import time
from typing import List

import colorama
import psutil
import pandas as pd
from sensor_msgs.msg import Image

list_of_processes = []

interrupt_requested = False


def get_args() -> argparse.Namespace:
    parser = argparse.ArgumentParser(
        description='This script calculates CPU and RAM usage of the provided command and its children')
    parser.add_argument('--exclude-list', nargs="+", default=['rviz'],
                        help='exclude processes containing the specified strings')
    parser.add_argument('command', type=str, help='Command to execute and evaluate')
    parser.add_argument('--output-file', type=str, default='timing_result.csv',
                        help='File where the result should be stored')
    return parser.parse_args()


def print_yellow(text: str) -> None:
    print(colorama.Fore.YELLOW + text + colorama.Style.RESET_ALL)


def sigint_handler(*_) -> None:
    print(colorama.Fore.RED + "Terminating the processes..." + colorama.Style.RESET_ALL)
    global list_of_processes, interrupt_requested
    for process in list_of_processes:
        parent = psutil.Process(process.pid)
        for child in parent.children(recursive=True):
            child.kill()
        parent.kill()
    interrupt_requested = True


signal.signal(signal.SIGINT, sigint_handler)
args = get_args()

command = args.command
list_of_processes = []
# Roscore is an essential part of every algorithm because they all run in ROS
list_of_processes.append(subprocess.Popen(['/bin/bash', '-c', 'roscore']))
time.sleep(5.0)
print('Executing ' + command)
evaluated_popen_object = subprocess.Popen(['/bin/bash', '-c', command], stdout=subprocess.DEVNULL,
                                          stderr=subprocess.STDOUT)
list_of_processes.append(evaluated_popen_object)
evaluated_process = psutil.Process(evaluated_popen_object.pid)
cpu_list = []
ram_list = []
time_list = []
starting_time = datetime.now()
while(not interrupt_requested):
    sum_cpu_usage = 0
    sum_ram_usage = 0

    sum_ram_usage += evaluated_process.memory_percent()
    sum_cpu_usage += evaluated_process.cpu_percent(interval=0.1)
    for child in evaluated_process.children(recursive=True):
        if(child.is_running()):
            try:
                if 'rviz' in child.name():
                    continue
                print(child.name())
                sum_cpu_usage += child.cpu_percent(interval=0.1)
                sum_ram_usage += child.memory_percent()
            except psutil.NoSuchProcess:
                print(f'no such process with pid {child.pid}')
    print(f'{sum_cpu_usage=},{sum_ram_usage=}')
    elapsed_seconds = (datetime.now() - starting_time).total_seconds()
    time_list.append(elapsed_seconds)
    cpu_list.append(sum_cpu_usage)
    ram_list.append(sum_ram_usage / 100 * (psutil.virtual_memory().total >> 20))  # in MB
    time.sleep(0.5)

output_df = pd.DataFrame(data=list(zip(time_list, cpu_list, ram_list)), columns=['timestamp', 'cpu', 'ram_mb'])
output_df.to_csv(args.output_file, index=False)
print_yellow(f'Saved the logged memory and cpu usage to {args.output_file}, exiting.')
